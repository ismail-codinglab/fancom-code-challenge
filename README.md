# Table of Contents

1. [Results](#results)
1. [Commands](#commands)
1. [Personal note](#personal-note)
1. [Technical decisions and why](#technical-decisions-and-why)
1. [Folder structure](#folder-structure)
1. [Improvements](#improvements)

# Results

### CI gitlab pipeline setup

![Alt text](./docs/gitlab-pipeline.png?raw=true "gitlab pipeline")

### Unit tests

![Alt text](./docs/test-coverage.png?raw=true "Jest test results")

### End2End tests

![Alt text](./docs/e2e-run.gif?raw=true "End2End results")

# Commands

### Serve application

`ng serve` or `npm run start` or `npm start`

### Build Angular app

`ng build` or `npm run build`

### Run tests

`ng test` or `npm run test` or `npm test`

### Run e2e tests

`ng e2e` or `npm run e2e`

_Note for CI reproducable e2e results run `npm run ci:e2e`_

# Technical decisions and why

### Karma becomes Jest

**What:** Automated (unit) testing tool
**Why:** It does not require a browser to run, is quite fast and shows you a nice report of the results. It's also one of the most used testing tool

### Not importing the whole angular material module

**Why:** This helps with reducing the bundle size and applying tree-shaking to get rid of unused parts

### Usage of lazy-loading

**Why:** This way the initial loading time is being reduced and only those parts that are needed will be loaded to make for a better user experience.

### Used Cypress for E2E tests

**What:** Automated End2End testing tool
**Why:** I find it a lot more developer friendly and easy to understand what is going wrong if you get a failed test

# Folder structure

```
 cypress/ <- Cypress End2End tests
  ...
 src/
    app/
      books/ <- a module with all it's relevant components, modules & services
      store/ <- All the NGXS stores
        books/ <- Books store
```

# Improvements

This is what can be added on top to improve the application:

- UI inputs with onPush change detection
- Internationalization
- Add performance tests
