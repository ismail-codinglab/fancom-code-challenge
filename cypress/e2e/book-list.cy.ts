describe('Book list', () => {
  beforeEach(() => {
    cy.visit('/');
  });
  it('Should load the page', () => {
    // make sure it's the correct page
    cy.contains('My Favorite Book List');
    cy.contains('My Books');
  });

  it('Should show books', () => {
    // check the first book
    cy.contains('HTML programmers exist');
    cy.contains('By: Hyper Lynx');
    cy.contains('On: January 17, 2023');

    // of course this wouldn't cover showing all
  });

  it('should be able to navigate to create a new book', () => {
    cy.url().should('not.include', 'books/new');
    cy.contains('+ New book').click();
    cy.url().should('include', 'books/new');
  });
});
