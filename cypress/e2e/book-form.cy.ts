describe('Book form', () => {
  beforeEach(() => {
    cy.visit('/books/new');
  });

  it('Should load the page', () => {
    // make sure it's the correct page
    cy.contains('My Favorite Book List');
    cy.contains('Add a new book');
  });

  it('Should be able to create a new book', () => {
    cy.get('[data-testid="title-input"]').type('cypress book');
    cy.get('[data-testid="author-input"]').type('cypress author');
    cy.get('[data-testid="publicationDate-input"]')
      .clear()
      .type('March 2, 2023');
    cy.get('[data-testid="submit-button"]').click();

    // check if the new book is added
    cy.contains('cypress book');
    cy.contains('By: cypress author');
    cy.contains('On: March 2, 2023');
  });
});
