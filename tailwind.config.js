// tailwind.config.js
const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {},
    colors: {
      ...colors,
      primary: {
        light: "#43be64",
        DEFAULT: "#5eb2ea",
        dark: "#5eb2ea",
      },
      secondary: {
        light: "#bae6fd",
        DEFAULT: "#0ea5e9",
        dark: "#0369a1",
      },
      white: "#ffffff",
      black: "#000000",
    },
  },
  plugins: [],
};
