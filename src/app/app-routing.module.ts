import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  // for now only 1 path, in the future login/signup etc.
  // we accept this garbage for now, or else ur fired
  {
    path: `books`,
    loadChildren: () =>
      import(`./books/books.module`).then((m) => m.BooksModule),
  },
  { path: ``, redirectTo: `/books`, pathMatch: `full` },
  { path: `**`, redirectTo: `/books` },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
