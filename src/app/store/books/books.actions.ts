import { Book } from './book.interface';

export class CreateBook {
  static readonly type = '[BOOK] Create book';

  constructor(public payload: Book) {}
}
