import { Action, Selector, State, StateContext } from '@ngxs/store';
import { CreateBook } from './books.actions';
import { Book } from './book.interface';
import data from '../../../assets/book-data.json';
import { Injectable } from '@angular/core';

function importBooks(
  data: { title: string; author: string; publicationDate: string }[]
) {
  let books = data.map((book) => ({
    ...book,
    publicationDate: new Date(book.publicationDate),
  }));

  return books;
}

interface BooksStateModel {
  books: Book[];
}

@State<BooksStateModel>({
  name: 'books',
  defaults: {
    books: importBooks(data),
  },
})
@Injectable()
export class BooksState {
  @Selector()
  static books(state: BooksStateModel): Book[] {
    return state.books;
  }

  @Action(CreateBook)
  createBook(ctx: StateContext<BooksStateModel>, action: CreateBook) {
    ctx.patchState({
      books: [...ctx.getState().books, action.payload],
    });
  }
}
