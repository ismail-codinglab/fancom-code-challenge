/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FormViewComponent } from './form-view.component';
import moment from 'moment';
import { NgxsModule, Store } from '@ngxs/store';
import { BooksState } from '../../store/books/books.state';
import {
  FormsModule,
  NonNullableFormBuilder,
  ReactiveFormsModule,
} from '@angular/forms';
import data from '../../../assets/book-data.json';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatNativeDateModule,
} from '@angular/material/core';
import { BooksRoutingModule } from '../books-routing.module';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';
import { ListViewComponent } from '../list-view/list-view.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

describe('FormViewComponent', () => {
  let component: FormViewComponent;
  let fixture: ComponentFixture<FormViewComponent>;
  let store: Store;
  let router: Router;
  let routerNavigateSpy: jest.SpyInstance;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormViewComponent],
      imports: [
        RouterTestingModule.withRoutes([
          { path: '', component: ListViewComponent },
          { path: '**', redirectTo: '' },
        ]),
        NgxsModule.forRoot([BooksState]),
        CommonModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDatepickerModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
        // application's root module. We provide it at the component level here, due to limitations of
        // our example generation script.
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    store = TestBed.inject(Store);
    router = TestBed.inject(Router);
    routerNavigateSpy = jest
      .spyOn(router, 'navigateByUrl')
      .mockImplementation();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Happy flow', () => {
    it(`should create a book with valid values`, () => {
      let initialState = store.selectSnapshot((state) => state.books);
      // expect the book store's state to be the same as the initial's book store's state
      expect(initialState.books).toEqual(
        data.map((book) => ({
          ...book,
          publicationDate: new Date(book.publicationDate),
        }))
      );
      let form = component.newBookForm;
      form.controls.author.setValue('Test man');
      form.controls.title.setValue('How to test');
      form.controls.publicationDate.setValue(moment());
      form.updateValueAndValidity();
      expect(form.valid).toBeTruthy();
      component.onSubmit();

      // I could expand this test to test all the cases like author with a value, and others not etc etc.
      // But you get the idea!

      let afterState = store.selectSnapshot((state) => state.books);
      // expect the book store's state to be the same as the initial's book store's state
      expect(afterState.books).toEqual([
        ...data.map((book) => ({
          ...book,
          publicationDate: new Date(book.publicationDate),
        })),
        {
          ...form.getRawValue(),
          publicationDate: form.controls.publicationDate.value.toDate(),
        },
      ]);
    });

    it(`should navigate back to home after creating a new book`, () => {
      let initialState = store.selectSnapshot((state) => state.books);

      // this ensures that only this test has been responsible for calling the navigate function
      expect(routerNavigateSpy).not.toHaveBeenCalled();

      // expect the book store's state to be the same as the initial's book store's state
      expect(initialState.books).toEqual(
        data.map((book) => ({
          ...book,
          publicationDate: new Date(book.publicationDate),
        }))
      );
      let form = component.newBookForm;
      form.controls.author.setValue('Test man');
      form.controls.title.setValue('How to test');
      form.controls.publicationDate.setValue(moment());
      form.updateValueAndValidity();
      expect(form.valid).toBeTruthy();
      component.onSubmit();

      // I could expand this test to test all the cases like author with a value, and others not etc etc.
      // But you get the idea!

      let afterState = store.selectSnapshot((state) => state.books);
      // expect the book store's state to be the same as the initial's book store's state
      expect(afterState.books).toEqual([
        ...data.map((book) => ({
          ...book,
          publicationDate: new Date(book.publicationDate),
        })),
        {
          ...form.getRawValue(),
          publicationDate: form.controls.publicationDate.value.toDate(),
        },
      ]);
      expect(routerNavigateSpy).toHaveBeenCalled();
    });
  });

  describe('Non-Happy flow', () => {
    it(`shouldn't create a book with invalid values`, () => {
      let initialState = store.selectSnapshot((state) => state.books);
      // expect the book store's state to be the same as the initial's book store's state
      expect(initialState.books).toEqual(
        data.map((book) => ({
          ...book,
          publicationDate: new Date(book.publicationDate),
        }))
      );
      let form = component.newBookForm;
      form.controls.author.setValue('');
      form.controls.title.setValue('');
      form.controls.publicationDate.setValue(moment(undefined));
      component.onSubmit();

      // I could expand this test to test all the cases like author with a value, and others not etc etc.
      // But you get the idea!

      let afterState = store.selectSnapshot((state) => state.books);
      // expect the book store's state to be the same as the initial's book store's state
      expect(afterState.books).toEqual(
        data.map((book) => ({
          ...book,
          publicationDate: new Date(book.publicationDate),
        }))
      );
    });
  });
});
