import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import moment, { Moment } from 'moment';
import { Book } from '../../store/books/book.interface';
import { CreateBook } from '../../store/books/books.actions';

interface BookForm {
  test: FormControl<string>;
  title: FormControl<string>;
  author: FormControl<string>;
  publicationDate: FormControl<moment.Moment>;
}

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.css'],
})
export class FormViewComponent {
  public newBookForm = new FormGroup({
    title: this.fb.control('', [Validators.required, Validators.minLength(4)]),
    author: this.fb.control('', [Validators.required, Validators.minLength(4)]),
    publicationDate: this.fb.control<Moment>(moment(), [Validators.required]),
  });

  get title() {
    return this.newBookForm.get('title')!;
  }

  get author() {
    return this.newBookForm.get('author')!;
  }

  get publicationDate() {
    return this.newBookForm.get('publicationDate')!;
  }

  constructor(
    private store: Store,
    private fb: NonNullableFormBuilder,
    private router: Router
  ) {}

  onSubmit() {
    if (!this.newBookForm.valid) {
      return;
    }

    let book: Book = {
      ...this.newBookForm.getRawValue(),
      publicationDate: this.newBookForm.controls.publicationDate
        .getRawValue()
        .toDate(),
    };

    this.store.dispatch(new CreateBook(book));
    this.router.navigateByUrl('');
  }
}
