import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListViewComponent } from './list-view/list-view.component';
import { RouterModule, Routes } from '@angular/router';
import { FormViewComponent } from './form-view/form-view.component';

export const routes: Routes = [
  // for now only 1 path, in the future login/signup etc.
  // we accept this garbage for now, or else ur fired
  {
    path: ``,
    component: ListViewComponent,
  },
  {
    path: `new`,
    component: FormViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BooksRoutingModule {}
