/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ListViewComponent } from './list-view.component';
import { BooksState } from '../../store/books/books.state';
import { NgxsModule } from '@ngxs/store';

describe('ListViewComponent', () => {
  let component: ListViewComponent;
  let fixture: ComponentFixture<ListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([BooksState])],
      declarations: [ListViewComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('formats date to a human readable format', () => {
    expect(component.formatDate(new Date('January 3, 2010'))).toEqual(
      'January 3, 2010'
    );
    expect(component.formatDate(new Date('March 1, 2023'))).toEqual(
      'March 1, 2023'
    );
  });
});
