import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Book } from '../../store/books/book.interface';
import { BooksState } from '../../store/books/books.state';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css'],
})
export class ListViewComponent implements OnInit {
  @Select(BooksState.books)
  books: Observable<Book[]>;

  constructor() {}

  ngOnInit() {}

  formatDate(date: Date): string {
    // Get the month name
    const options: { month: 'long' } = { month: 'long' };
    const month = date.toLocaleString('en-US', options);

    // Get the day and year
    const day = date.getDate();
    const year = date.getFullYear();

    // Combine the parts to form the desired format
    const formattedDate = `${month} ${day}, ${year}`;

    return formattedDate;
  }
}
